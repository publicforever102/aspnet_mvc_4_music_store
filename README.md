﻿# Steps to make this project up and running:
Edit the connection string in web.config to match your local database.
* For example, mine is:
```xml
<connectionStrings>
    <!--Use Windows Auth.-->
    <!--<add name="AspNet_Mvc4_Music_Store" connectionString="Data Source=.;Initial Catalog=AspNet_Mvc4_Music_Store;Integrated Security=true" providerName="System.Data.SqlClient"/>-->
    <!--Use sa account.-->
    <add name="AspNet_Mvc4_Music_Store" connectionString="server=.;database=AspNet_Mvc4_Music_Store;uid=sa;password=123456;MultipleActiveResultSets=true;" providerName="System.Data.SqlClient" />
</connectionStrings>
```

Then use Entity Framework tool to create database:
* Open the project in VS -> Open the cmd `Package Manager Console` -> Run cmd: `update-database`
* If there's no error, open SSMS to see if database `AspNet_Mvc4_Music_Store` is created.

Run the project.