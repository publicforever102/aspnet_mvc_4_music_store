﻿using System.Web;
using System.Web.Mvc;

namespace AspNet_Mvc_4_Music_Store
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
