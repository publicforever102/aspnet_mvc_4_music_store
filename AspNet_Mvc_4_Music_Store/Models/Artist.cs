﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AspNet_Mvc_4_Music_Store.Models
{
    public class Artist
    {
        public int ArtistId { get; set; }
        public string Name { get; set; }
    }
}