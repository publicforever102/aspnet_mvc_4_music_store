﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AspNet_Mvc_4_Music_Store.Models
{
    public class Genre
    {
        public int GenreId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        // Relationship.
        public List<Album> Albums { get; set; }
    }
}