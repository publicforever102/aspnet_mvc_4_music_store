﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AspNet_Mvc_4_Music_Store.Models
{
    public class MusicStoreEntities : DbContext
    {
        public MusicStoreEntities() : base("AspNet_Mvc4_Music_Store")
        { }

        public DbSet<Album> Albums { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Artist> Artists { get; set; }
    }
}