﻿using AspNet_Mvc_4_Music_Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspNet_Mvc_4_Music_Store.Controllers
{
    public class HomeController : Controller
    {
        MusicStoreEntities dbContext = new MusicStoreEntities();
        // GET: Home
        public ActionResult Index()
        {
            // Seed sample data if db is new.
            SampleData.Seed(dbContext);

            return View();
        }
    }
}