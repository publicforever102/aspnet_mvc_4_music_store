﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

// My custom namspaces.
using AspNet_Mvc_4_Music_Store.Models;

namespace AspNet_Mvc_4_Music_Store.Controllers
{
    public class StoreController : Controller
    {
        MusicStoreEntities storeDB = new MusicStoreEntities();

        // GET: Store
        public ActionResult Index()
        {
            var genres = storeDB.Genres.ToList();

            //var genres = new List<Genre>
            //{
            //    new Genre { Name = "Disco"},
            //    new Genre { Name = "Jazz"},
            //    new Genre { Name = "Rock"}
            //};

            return View(genres);
         }

        public ActionResult Details(int id)
        {
            var album = storeDB.Albums.Find(id);
            //var album = new Album { Title = "Album " + id };

            return View(album);
        }

        public ActionResult Browse(string genreName)
        {
            // Retrieve Genre and its Associated Albums from database
            var genreModel = storeDB.Genres.Include("Albums").Single(item => item.Name == genreName);

            //var genreModel = new Genre { Name = genre };
            return View(genreModel);
        }
    }
}